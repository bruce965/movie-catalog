﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MovieCatalog.Logic;

namespace MovieCatalog.UI
{
	public partial class MainForm : Form
	{
#if DEBUG
		readonly MovieFinder finder = new MovieFinder("/media/mint/8776d0a3-875a-47e9-8f26-33b261fb834a/Film");
#else
		readonly MovieFinder finder = new MovieFinder(".");
#endif
		
		public MainForm() {
			InitializeComponent();
			
			update();
		}
		
		void update() {
			SuspendLayout();
			
			backButton.Visible = !finder.IsRoot;
			
			flowLayoutPanel.Controls.Clear();
			foreach (var entry in finder.Entries) {
				var uiEntry = new CatalogEntry(entry);
				
				uiEntry.LinkFollow += (sender, e) => {
					var senderEntry = ((CatalogEntry)sender).Entry;
					
					var movieEntry = senderEntry as MovieEntry;
					if (movieEntry != null) {
						movieEntry.Play();
					} else {
						finder.Browse(
							Path.GetFileName(senderEntry.Path)
						);
						update();
					}
				};
				
				flowLayoutPanel.Controls.Add(uiEntry);
			}

			ResumeLayout();
		}
		
		void BackButtonClick(object sender, EventArgs e) {
			finder.Browse("..");
			update();
		}
	}
}
