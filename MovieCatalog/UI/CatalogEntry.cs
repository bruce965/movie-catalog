﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MovieCatalog.Logic;
using MovieCatalog.Tools;

namespace MovieCatalog.UI
{
	public partial class CatalogEntry : UserControl
	{
		public readonly Entry Entry;
		
		public object Value { get; set; }
		
		public event EventHandler LinkFollow;
		
		public CatalogEntry(Entry entry) {
			this.Entry = entry;
			
			SuspendLayout();

			InitializeComponent();
			
			label.Text = entry.Title;
			label.Font = new Font(FontFinder.GetFirstAvailable("TakaoPGothic", "MS PGothic"), 10);
			pictureBox.Image = entry.CoverPath == null ? null : ImageResizer.ResizeImage(Image.FromFile(entry.CoverPath), pictureBox.Width, pictureBox.Height);
			
			if (entry is MovieEntry) {
				label.BackColor = Color.LightBlue;
				pictureBox.BackColor = Color.Black;
			}

			if (entry.Title.Contains("TODO")) {
				label.BackColor = Color.LightPink;
			}
			
			ResumeLayout();
		}
		
		void CatalogEntryClick(object sender, EventArgs e) {
			LinkFollow.Invoke(this, e);
		}
	}
}
