﻿/*
 * Created by SharpDevelop.
 * User: Fabio Iotti
 * Date: 2015-09-06
 * Time: 13:21
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace MovieCatalog.UI
{
	partial class CatalogEntry
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.PictureBox pictureBox;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.label.Location = new System.Drawing.Point(0, 117);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(200, 23);
			this.label.TabIndex = 0;
			this.label.Text = "label";
			this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label.Click += new System.EventHandler(this.CatalogEntryClick);
			// 
			// pictureBox
			// 
			this.pictureBox.BackColor = System.Drawing.Color.White;
			this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox.Location = new System.Drawing.Point(0, 0);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(200, 117);
			this.pictureBox.TabIndex = 1;
			this.pictureBox.TabStop = false;
			this.pictureBox.Click += new System.EventHandler(this.CatalogEntryClick);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			// 
			// CatalogEntry
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.pictureBox);
			this.Controls.Add(this.label);
			this.Cursor = System.Windows.Forms.Cursors.Hand;
			this.Name = "CatalogEntry";
			this.Size = new System.Drawing.Size(200, 140);
			this.Click += new System.EventHandler(this.CatalogEntryClick);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.ResumeLayout(false);

		}
	}
}
