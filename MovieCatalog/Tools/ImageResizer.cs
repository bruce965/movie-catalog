using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace MovieCatalog.Tools
{
	public static class ImageResizer
	{
		// preserves aspect ratio
		public static Bitmap ResizeImage(Image image, int width, int height)
		{
			var ratioo = (float)image.Width / image.Height;
			var ratioi = (float)width / height;
			var rr = ratioo / ratioi;
			width = (int)(width * rr);

			var destRect = new Rectangle(0, 0, width, height);
			var destImage = new Bitmap(width, height, PixelFormat.Format32bppRgb);

			destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

			using (var graphics = Graphics.FromImage(destImage))
			{
				graphics.CompositingMode = CompositingMode.SourceCopy;
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

				using (var wrapMode = new ImageAttributes())
				{
					wrapMode.SetWrapMode(WrapMode.TileFlipXY);
					graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
				}
			}

			return destImage;
		}
	}
}

