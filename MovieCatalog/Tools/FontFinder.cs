using System;
using System.Drawing;
using System.Linq;

namespace MovieCatalog
{
	// https://bitbucket.org/bruce965/japanese-keyboard/src/master/FontFinder.cs
	public static class FontFinder
	{
		public static string GetFirstAvailable(params string[] fontNames) {
			foreach (var fontName in fontNames)
				using (var font = new Font(fontName, 12))
					if (fontName == font.Name)
						return fontName;

			return fontNames.Last();
		}
	}
}

