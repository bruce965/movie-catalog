﻿using System;
using System.Windows.Forms;
using MovieCatalog.UI;

namespace MovieCatalog
{
	sealed class Program
	{
		[STAThread]
		static void Main(string[] args) {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
	}
}
