﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using System.IO;

namespace MovieCatalog.Logic
{
	public class MovieEntry : Entry
	{
		public void Play() {
			if (Directory.Exists(Path) && !IsDVD(Path)) {
				var audioFiles = Directory.GetFiles(Path).Audio();

				Process.Start(
					"vlc",
					"--sub-autodetect-file" +
					((audioFiles.Count() == 0) ? "" : " --input-slave \"" + String.Join("#", audioFiles) + "\"") +
					" \"" + Directory.GetFiles(Path).Movies().First() + "\""
				);
			} else {
				Process.Start("vlc", "\"" + Path + "\"");
			}
		}

		public static bool IsDVD(string directory) {
			var audioTs = false;
			var videoTs = false;

			if (Directory.GetFiles(directory).Movies().Count() > 0)
				return false;

			foreach (var subdir in Directory.GetDirectories(directory)) {
				switch (System.IO.Path.GetFileName(subdir)) {
				case "AUDIO_TS":
					audioTs = true;
					continue;
				case "VIDEO_TS":
					videoTs = true;
				continue;
					default:
					return false;
				}
			}

			return audioTs && videoTs;
		}
	}
}
