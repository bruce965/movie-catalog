﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MovieCatalog.Logic
{
	public class MovieFinder
	{
		string currentPath;
		
		public MovieFinder(string initialPath) {
			currentPath = Path.GetFullPath(initialPath);
		}
		
		public bool IsRoot {
			get { return Directory.GetParent(currentPath) == null; }
		}
		
		public IEnumerable<Entry> Entries {
			get {
				foreach (var directory in Directory.GetDirectories(currentPath)) {
					if (MovieEntry.IsDVD(directory) || (Directory.GetFiles(directory).Movies().Count() == 1 && Directory.GetDirectories(directory).Count() == 0)) {
						yield return new MovieEntry {
							Path = directory,
							CoverPath = Directory.GetFiles(directory).Images().FirstOrDefault(),
							Title = Path.GetFileNameWithoutExtension(directory)
						};
					} else {
						yield return new Entry {
							Path = directory,
							CoverPath = Directory.GetFiles(directory).Images().FirstOrDefault(),
							Title = Path.GetFileName(directory)
						};
					}
				}
				
				foreach (var movie in Directory.GetFiles(currentPath).Movies()) {
					yield return new MovieEntry {
						Path = movie,
						Title = Path.GetFileNameWithoutExtension(movie)
					};
				}
			}
		}
		
		public void Browse(string name) {
			currentPath = name == ".." ? Directory.GetParent(currentPath).FullName : Path.Combine(currentPath, name);
		}


	}
}
