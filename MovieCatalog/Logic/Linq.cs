﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MovieCatalog.Logic
{
	public static class Linq
	{
		readonly static string[] movieExtensions = {
			"mp4", "flv", "mpg", "mpeg", "avi", "mov", "mkv"
		};
		
		readonly static string[] imageExtensions = {
			"jpg", "jpeg", "png", "bmp", "gif"
		};

		readonly static string[] audioExtensions = {
			"mp3", "aac", "flac", "ogg"
		};
		
		public static IEnumerable<string> Movies(this IEnumerable<string> files) {
			foreach (var file in files)
				if (movieExtensions.Contains(Path.GetExtension(file).TrimStart('.')))
					yield return file;
		}
		
		public static IEnumerable<string> Images(this IEnumerable<string> files) {
			foreach (var file in files)
				if (imageExtensions.Contains(Path.GetExtension(file).TrimStart('.')))
					yield return file;
		}

		public static IEnumerable<string> Audio(this IEnumerable<string> files) {
			foreach (var file in files)
				if (audioExtensions.Contains(Path.GetExtension(file).TrimStart('.')))
					yield return file;
		}

		public static IEnumerable<T> IgnoreExceptions<T>(this IEnumerable<T> ienu) {
			var enumerator = ienu.GetEnumerator();
			
			do {
				T entry;
				
				try {
					entry = enumerator.Current;
				} catch {
					break;
				}
				
				yield return entry;
			} while(enumerator.MoveNext());
			
			enumerator.Dispose();
		}
	}
}
