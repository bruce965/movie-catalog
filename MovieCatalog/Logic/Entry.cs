﻿using System;

namespace MovieCatalog.Logic
{
	public class Entry
	{
		public string Path;
		public string CoverPath;
		
		public string Title;
	}
}
